﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;

namespace RSDLib
{
    [Serializable]
    public class Team
    {
        public int TeamID;
        public int UserID;
        public int StatusID;
        public string LeagueName;
        public string Name;
        public int CityID;
        public string City;

        public int Color1;
        public int Color2;

        public List<Player> Players;

        public Team()
        {
            Players = new List<Player>();
        }

        public Team(int _TeamID)
        {
            TeamID = _TeamID;
            LoadTeam();
        }

        public void LoadTeam()
        {
            DataHelper db = new DataHelper("spGetTeam");
            db.Command.Parameters.AddWithValue("@TeamID", TeamID);

            foreach (DataRow dr in db.GetDataSet().Tables[0].Rows)
            {
                TeamID = (int)dr["TeamID"];
                if (dr["UserID"] != DBNull.Value)
                    UserID = (int)dr["UserID"];
                Name = dr["TeamName"].ToString();
                City = dr["CityName"].ToString();

                if (dr["CityID"] != DBNull.Value)
                    CityID = (int)dr["CityID"];
                if (dr["Color1"] != DBNull.Value)
                    Color1 = (int)dr["Color1"];
                if (dr["Color2"] != DBNull.Value)
                    Color2 = (int)dr["Color2"];
                LeagueName = dr["LeagueName"].ToString();
                StatusID = (int)dr["StatusID"];
            }
        }

        public static DataTable GetTeams(int WorldID)
        {
            DataHelper db = new DataHelper("spGetTeams");
            db.Command.Parameters.AddWithValue("@WorldID", WorldID);
            return db.GetDataSet().Tables[0];
        }

        public static DataTable GetCities()
        {
            DataHelper db = new DataHelper("spGetCities");
            return db.GetDataSet().Tables[0];
        }

        public static void UpdateTeam(int TeamID, int UserID)
        {
            UpdateTeam(TeamID, UserID, "", -1, -1, -1);
        }

        public static void UpdateTeam(int TeamID, int UserID, string TeamName, int CityID, int Color1, int Color2)
        {
            DataHelper db = new DataHelper("spUpdateTeam");
            db.Command.Parameters.AddWithValue("@TeamID", TeamID);
            if (UserID > 0)
                db.Command.Parameters.AddWithValue("@UserID", UserID);
            if (TeamName != "")
                db.Command.Parameters.AddWithValue("@TeamName", TeamName);
            if (CityID > 0)
                db.Command.Parameters.AddWithValue("@CityID", CityID);
            if (Color1 > 0)
                db.Command.Parameters.AddWithValue("@Color1", Color1);
            if (Color2 > 0)
                db.Command.Parameters.AddWithValue("@Color2", Color2);

            db.Execute();
            
        }
    }

    public enum LeagueEnum
    {
        AL,
        NL
    }
}
