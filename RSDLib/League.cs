﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSDLib
{
    /// <summary>
    /// Summary description for League
    /// </summary>
    public class League
    {
        public int LeagueID;
        public string Name;

        public List<Team> Teams;

        public League()
        {
            Teams = new List<Team>();
        }
    }
}