﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace RSDLib
{
    /// <summary>
    /// Summary description for DataHelper
    /// </summary>
    public class DataHelper
    {
        public SqlCommand Command;
        string DBString = DBStrings.dbLive;

        public DataHelper(string CommandText)
        {
            Command = new SqlCommand();
            Command = GetCommand(CommandText);
        }

        public SqlCommand GetCommand(string CommandText)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings[DBString].ToString());
                cmd.Connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = CommandText;

                return cmd;
            }
        }

        public DataSet GetDataSet()
        {
            SqlDataAdapter da = new SqlDataAdapter(Command);

            DataSet ds = new DataSet();

            da.Fill(ds);

            da.Dispose();

            Command.Connection.Close();

            return ds;
        }

        public void Execute()
        {
            Command.ExecuteNonQuery();
            Command.Connection.Close();
        }

        public int ExecuteWithIntReturn()
        {
            int intReturn = (int)Command.ExecuteScalar();
            Command.Connection.Close();
            return intReturn;
        }
    }

    public static class DBStrings
    {
        public static string dbLive = "dbLive";
    }
}