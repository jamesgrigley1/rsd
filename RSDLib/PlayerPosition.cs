﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSDLib
{
    public class PlayerPosition
    {
        public int PositionID;
        public string Position;

        public static DataTable GetPositions()
        {
            DataHelper db = new DataHelper("spGetPositions");
            return db.GetDataSet().Tables[0];
        }
    }
}
