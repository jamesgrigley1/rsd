﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSDLib
{
    [Serializable]
    public class Player
    {
        public int PlayerID;
        public string FirstName;
        public string LastName;
        public PlayerStatusEnum Status;

        public PositionEnum Position;

        public PlayerAttributes Attributes;

        public PlayerStats Stats;

        public PlayerCareer Career;

        public Player()
        {
            Attributes = new PlayerAttributes();
        }

        public static DataTable GetTeams(int WorldID)
        {
            DataHelper db = new DataHelper("spGetFreeAgents");
            return db.GetDataSet().Tables[0];
        }

        public static DataTable GetTeamPlayers(int TeamID)
        {
            DataHelper db = new DataHelper("spGetTeamPlayers");
            db.Command.Parameters.AddWithValue("@TeamID", TeamID);
            return db.GetDataSet().Tables[0];
        }

        public static void HirePlayer(int PlayerID, int TeamID)
        {
            DataHelper db = new DataHelper("spHirePlayer");
            db.Command.Parameters.AddWithValue("@PlayerID", PlayerID);
            db.Command.Parameters.AddWithValue("@TeamID", TeamID);
            db.Execute();
        }

        public static void UpdatePosition(int PlayerID, int PositionID)
        {
            DataHelper db = new DataHelper("spUpdatePosition");
            db.Command.Parameters.AddWithValue("@PlayerID", PlayerID);
            if (PositionID >= 0)
                db.Command.Parameters.AddWithValue("@PositionID", PositionID);
            db.Execute();
        }
    }

    public enum PositionEnum
    {
        Pitcher = 0,
        Catcher = 1,
        FirstBase = 2,
        SecondBase = 3,
        ThirdBase = 4,
        ShortStop = 5,
        LeftField = 6,
        CenterField = 7,
        RightField = 8
    }

    public enum PlayerStatusEnum
    {
        Owned,
        Draft,
        FreeAgent
    }
}
