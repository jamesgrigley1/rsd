﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;


/// <summary>
/// Summary description for Utility
/// </summary>
public static class Utility
{
    public static byte[] EncryptPassword(string Password)
    {
        byte[] hashedDataBytes = null;

        UTF8Encoding encoder = new UTF8Encoding();

        MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

        hashedDataBytes = md5Hasher.ComputeHash(encoder.GetBytes(Password));

        return hashedDataBytes;
    }

    public static void SendEmail(string MailTo, string Subject, string Message)
    {
        const string MAIL_SERVER = "localhost";

        using (MailMessage msg = new MailMessage("", MailTo))
        {
            msg.Subject = Subject;
            msg.Priority = MailPriority.High;
            msg.IsBodyHtml = false;
            msg.Body = Message; // "Notes:\n\r" + txtNotes.Text + "\n\r\n\rError:\n\r" + sErr;

            SmtpClient smtp = new SmtpClient(MAIL_SERVER);
            smtp.Send(msg);
        }
    }
}