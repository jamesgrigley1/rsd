﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSDLib
{
    /// <summary>
    /// Summary description for PlayerAttributes
    /// </summary>
    public class PlayerAttributes
    {
        public int BatContact; // ability to make contact with ball
        public int BatPower; //determines how well a player hits the ball and plays the primary role in slugging percentage.

        public int PitchAccuracy;
        public int PitchPower;
        //public int BattingVsLeft; //Batting Versus Left-Handed Pitching determines how a batter fares against lefties, both in terms of making contact and power.
        //public int BattingVsRight; // Batting Versus Right-Handed Pitching determines how a batter fares against righties, both in terms of making contact and power.
        //public int BattingEye; // involves plate recognition and dictates a player's ability to draw the walk, avoid the strike out looking, and taking advantage of mistake pitches.
        //public int BaseRunning; // determines how adept the player is at running the bases. This includes taking an extra base or holding up, and knowing when and when not to steal a bag.
        //public int Bunting; //  indicator of how well the player is at laying down sacrifice bunts as well as laying down bunt singles.
        //public int PushPull; // indicates whether the player is a pull hitter or opposite field hitter. A rating of 0 indicates an extreme pull hitter.
        //public int Cluth;// CLUTCH, ability to perform in pressure situation.
        //public int Range; // how mobile a player is in the field, how skilled at positioning himself in the field for different hitters, and plays a key role in determining  what position the player is capable of playing.
        //public int Glove; // rating indicates how skilled the player is at fielding balls
        //public int ArmStrength;
        //public int ArmAccuracy; //used in every fielding play that involves a throw
        //public int PitchCalling; //how well the player handles pitchers behind the plate.
        //public int Durability; //  determines how much time off a guy needs during the course of  a season and how quickly he can bounce back between pitching appearances
        //public int Health; // HEALTH, dictates a player's ability to avoid injury. A rating of 100 indicates  a player that is incredibly durable.
        //public int Speed; //  how fast the player is.
        //public int Patience; //  plays a role in a player's ability to deal with adversity following demotions, injuries and lack of promotions.
        //public int Temper; // how well a player deals with spur-of-the-moment adversity such as being hit by a pitch or called out on strikes
        //public int Makeup; // similar to work ethic and plays a key role in a player's ability to improve and hold off decline.

        //public int OverallRating // combines a player's overall skill set into one value
        //{
        //    get { return Power + BattingVsLeft; } //or w/e 
        //}

        public PlayerAttributes()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}