﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace RSDLib
{
    /// <summary>
    /// Summary description for World
    /// </summary>
    public class World
    {
        int WorldID;
        string Name;
        public League League;

        public World()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static DataTable GetOpenWorlds()
        {
            return new DataHelper("spGetOpenWorlds").GetDataSet().Tables[0];
        }
    }
}