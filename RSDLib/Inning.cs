﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSDLib
{
    public class Inning
    {
        public int InningID;
        public int Number;
        public InningTypeEnum Type;
        public List<PlayerStats> Stats;

        public Inning()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }

    public enum InningTypeEnum
    {
        Top,
        Bottom
    }
}
