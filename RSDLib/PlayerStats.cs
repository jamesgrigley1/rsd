﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RSDLib
{
    /// <summary>
    /// Summary description for PlayerStats
    /// </summary>
    public class PlayerStats
    {
        public int PlayerStatsID;
        public PlayerStatModeEnum Mode;

        public int GamesPlayed;
        public int AtBats;
        public int Runs;
        public int Hits;
        public int Doubles;
        public int Triples;
        public int HomeRuns;
        public int RunsBattedIn;
        public int StolenBases;
        public int CaughtStealing;
        public int Walks;
        public int Strikeouts;
        public int HitByPitch;
        public int SacraficeHits;
        public int SacraficeFlies;
        public int IntentionalBasesOnBalls;


        public PlayerStats()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }

    public enum PlayerStatModeEnum
    {
        Game,
        Season,
        Career
    }
}