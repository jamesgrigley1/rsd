﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSDLib
{
    public class UserAccount
    {
        public long UserID;
        public string UserName;
        public string FirstName;
        public string LastName;
        public string EmailAddress;
        public int CurrentWorldID;
        public string CurrentWorld;
        public int CurrentTeamID;
        public string CurrentTeamName;


        public UserAccount(string _UserName, string Password)
        {
            DataHelper db = new DataHelper("spUserLogin");

            db.Command.Parameters.AddWithValue("@UserName", _UserName);
            db.Command.Parameters.AddWithValue("@Password", Utility.EncryptPassword(Password));

            foreach (DataRow dr in db.GetDataSet().Tables[0].Rows)
            {
                UserID = (int)dr["UserID"];
                UserName = dr["UserName"].ToString();
                CurrentTeamID = (int)dr["TeamID"];
                CurrentTeamName = dr["TeamName"].ToString();
                CurrentWorldID = (int)dr["WorldID"];
                CurrentWorld = dr["WorldName"].ToString();
            }
        } 
    }
}
