﻿--======================================
--== Tables
--======================================

Create Table UserAccounts
(
	UserID Int Identity(1,1) Primary Key NOT NULL
	,FirstName NVarChar(200)
	,LastName NVarChar(200)
	,UserName VarChar(50)
	,EmailAddress VarChar(200)
	,Password Binary(16)
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
	,LastLogin DateTime Default (GETDATE()) NOT NULL
)
GO

-- a league may be the same as a world...idk
Drop Table Worlds
Create Table Worlds
(
	WorldID Int Identity(1,1) Primary Key NOT NULL
	,StatusID Int Default(0) NOT NULL
	,Name VarChar(50)
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table Leagues
(
	LeagueID Int Identity(1,1) Primary Key NOT NULL
	,WorldID Int
	,LeagueName VarChar(100)
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table Seasons
(
	SeasonID Int Identity(1,1) Primary Key NOT NULL
	,LeagueID Int
	,SeasonNumber Int
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table Games
(
	GameID Int Identity(1,1) Primary Key NOT NULL
	,SeasonID Int
	,HomeTeamID Int
	,AwayTeamID Int
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table Innings
(
	InningID Int Identity(1,1) Primary Key NOT NULL
	,GameID Int
	,InningNumber Int
	,TeamID Int -- maybe do this differnt
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table InningStats
(
	InningStatID Int Identity(1,1) Primary Key NOT NULL
	,InningID Int
	,PlayerID Int
	,AtBats Int
    ,Runs Int
    ,Hits Int
    ,Doubles Int
    ,Triples Int
    ,HomeRuns Int
    ,RunsBattedIn Int
    ,StolenBases Int
    ,CaughtStealing Int
    ,Walks Int
    ,Strikeouts Int
    ,HitByPitch Int
    ,SacraficeHits Int
    ,SacraficeFlies Int
    ,IntentionalBasesOnBalls Int
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table Teams
(
	TeamID Int Identity(1,1) Primary Key NOT NULL
	,WorldID Int NOT NULL
	,UserID Int NULL
	,TeamName VarChar(50)
	,City VarChar(50)
	,Color1 Int
	,Color2 Int
	,LeagueTypeID Int
	,StatusID Int Default(0)
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO



Create Table Players
(
	PlayerID Int Identity(1,1) Primary Key NOT NULL
	,StatusID Int
	,TeamID Int NULL
	,PositionID Int NULL
	,FirstName VarChar(50)
	,LastName VarChar(50)
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table PlayerAttributes
(
	PlayerAttributeID Int Identity(1,1) Primary Key NOT NULL
	,Contact Int,
    ,Power Int,
    ,BattingVsLeft Int
    ,BattingVsRight Int
    ,BattingEye Int
    ,BaseRunning Int
    ,Bunting Int
    ,PushPull Int
    ,Cluth Int
    ,Range Int
    ,Glove Int
    ,ArmStrength Int
    ,ArmAccuracy Int
    ,PitchCalling Int
    ,Durability Int
    ,Health Int
    ,Speed Int
    ,Patience Int
    ,Temper Int
    ,Makeup Int
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

Create Table PlayerStats
(
	ID Int Identity(1,1) Primary Key NOT NULL
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO

--TEMPLATE
Create Table 
(
	ID Int Identity(1,1) Primary Key NOT NULL
	,InsertDate DateTime Default (GETDATE()) NOT NULL
	,UpdateDate DateTime Default (GETDATE()) NOT NULL
)
GO


--======================================
--== Procedures
--======================================
Create Procedure spUserAdd
	@EmailAddress VarChar(200)
	,@FirstName nVarChar(200)
	,@LastName nVarChar(200)
	,@UserName VarChar(200)
	,@Password Binary(16)
As
	Set NoCount ON
	
	If Not Exists(Select EmailAddress From UserAccounts Where EmailAddress=@EmailAddress OR UserName=@UserName)
	 Begin
		Insert Into UserAccounts
		(
			FirstName,
			LastName,
			UserName,
			EmailAddress,
			Password
		)Values(
			@FirstName,
			@LastName,
			@UserName,
			@EmailAddress
			@Password
		)
	  End
	  
	  Select * From UserAccounts Where UserID = Scope_Identity()
GO	
	
Create Procedure spUserLogin
	@UserName VarChar(200)
	,@Password Binary(16)
As
	Set NoCount ON
	
	Select
		*
	From
		UserAccounts
	Where
		(EmailAddress=@UserName OR UserName=@UserName)
		AND Password=@Password
GO

Create Procedure spGetOpenWorlds
As
	Set NoCount ON
	
	Select 
		w.WorldID,
		WorldName,
		Count(*) as OpenTeams 
	From Worlds w
		Inner Join Teams t on w.WorldID=t.WorldID
	Where 
		w.StatusID=0
		AND t.UserID IS NULL
	Group By w.WorldID, WorldName
GO

Create Procedure spAddWorld
	@WorldName VarChar(50)
As
	Set NoCount ON
	
	Declare 
		@WorldID Int
		,@x Int = 1 
		,@LeagueTypeID Int = 1
	
	Insert Into Worlds (WorldName) Values(@WorldName)
	
	Set @WorldID = SCOPE_IDENTITY()
		
	While @x <= 32
	 Begin
		If @x = 17
			Set @LeagueTypeID = 2
			
		Exec spAddTeam @WorldID=@WorldID, @LeagueTypeID=@LeagueTypeID, @Color1=1
		
		Set @x = @x + 1
	 End
GO

Create Procedure spAddTeam
	@WorldID Int
	,@UserID Int = NULL
	,@TeamName VarChar(50) = NULL
	,@City VarChar(50) = NULL
	,@Color1 Int = NULL
	,@Color2 Int 
	,@LeagueTypeID Int
As
	Set NoCount ON
	
	Insert Into Teams
	(
		WorldID
		,LeagueTypeID
	)Values(
		@WorldID
		,@LeagueTypeID
	)
GO

Create Procedure spGetTeams
	@WorldID Int
As
	Set NoCount ON
	
	Select
		TeamID,
		TeamName,
		City,
		Case
			WHEN t.UserID IS NULL THEN 'Open'
			Else 'Closed'
		End as Status,
		u.UserName as Owner
	From
		Teams t
		Left Join UserAccounts u ON t.UserID=u.UserID
GO
	
	