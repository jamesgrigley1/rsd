﻿using RSDLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberMaster : System.Web.UI.MasterPage
{
    UserAccount user;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack && !IsTester() && !Request.Url.ToString().Contains("Closed"))
        {
            Response.Redirect("~/Closed.aspx");
        }

        if (Session["UserAccount"] != null)
        {
            user = (UserAccount)Session["UserAccount"];
        }

        if (!IsPostBack)
        {
            if (user.UserName != "")
            {
                Label lbl = (Label)HeadLoginView.FindControl("lblUserName");
                lbl.Text = user.UserName;
            }
        }

    }

    public void HideNav()
    {
        NavigationMenu.Visible = false;
    }

    private bool IsTester()
    {
        HttpCookie myCookie = Request.Cookies["IsTester"];
        if (myCookie == null)
        {
            return false;
        }

        //ok - cookie is found.
        //Gracefully check if the cookie has the key-value as expected.
        if (!string.IsNullOrEmpty(myCookie.Values["Tester"]))
        {
            return true;
        }

        return false;
    }
}