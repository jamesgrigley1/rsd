﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Closed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SiteMaster master = (SiteMaster)this.Master;
        master.HideNav();
    }
    protected void btnLogin_OnClick(object sender, EventArgs e)
    {
        Login();
    }
    private void Login()
    {
        if (txtTesterLogin.Text == "oldeagle")
        {
            HttpCookie myCookie = new HttpCookie("IsTester");

            //Add key-values in the cookie
            myCookie.Values.Add("Tester", "true");

            //set cookie expiry date-time. Made it to last for next 12 hours.
            myCookie.Expires = DateTime.Now.AddDays(365);

            //Most important, write the cookie to client.
            Response.Cookies.Add(myCookie);

            Response.Redirect("~/Default.aspx");
        }
    }
}