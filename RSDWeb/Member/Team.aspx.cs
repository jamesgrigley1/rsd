﻿using RSDLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_Team : System.Web.UI.Page
{
    Team team = new Team();
    private DataTable dtPositions; 

    protected void Page_Load(object sender, EventArgs e)
    {
        ParseQueryString();
        dtPositions = PlayerPosition.GetPositions();


        if (!IsPostBack) { 
            BindDropdowns();
            if (team.TeamID > 0)
            {
                team.LoadTeam();
                ViewState["Team"] = team;
                if (team.StatusID == 0)
                {
                    pnlOpen.Visible = true;
                    pnlTeam.Visible = false;
                    pnlEditTeam.Visible = false;
                }
                else
                {
                    pnlOpen.Visible = false;
                    pnlTeam.Visible = true;
                    pnlEditTeam.Visible = false;
                    BindDisplay();
                }
            }
        }
        else
        {
            team = (Team)ViewState["Team"];
        }
    }

    private void BindDropdowns()
    {
        ddCity.DataSource = Team.GetCities();
        ddCity.DataValueField = "CityID";
        ddCity.DataTextField = "CityName";
        ddCity.DataBind();

    }

    private void ParseQueryString()
    {
        if (Request.QueryString["t"] != null && Request.QueryString["t"] != "")
        {
            team.TeamID = int.Parse(Request.QueryString["t"].ToString());
        }
    }

    private void BindDisplay()
    {
        team.LoadTeam();
        lblLeague_d.Text = team.LeagueName;
        lblTeamName_d.Text = team.Name;
        lblCity_d.Text = team.City;

        gvPlayers.DataSource = Player.GetTeamPlayers(team.TeamID);
        gvPlayers.DataBind();
    }

    private void BindForm()
    {
        team.LoadTeam();
        lblLeague.Text = team.LeagueName;
        txtTeamName.Text = team.Name;
        ddCity.SelectedValue = team.CityID.ToString();


    }
    protected void btnPurchaseTeam_Click(object sender, EventArgs e)
    {
        Team.UpdateTeam(team.TeamID, int.Parse(User.Identity.Name));
        pnlOpen.Visible = false;
        pnlTeam.Visible = false;
        pnlEditTeam.Visible = true;
        BindForm();
    }
    protected void btnUpdateTeam_Click(object sender, EventArgs e)
    {
        Team.UpdateTeam(team.TeamID, int.Parse(User.Identity.Name), txtTeamName.Text, int.Parse(ddCity.SelectedValue.ToString()), 0, 0);
        pnlOpen.Visible = false;
        pnlTeam.Visible = true;
        pnlEditTeam.Visible = false;
        BindDisplay();
    }

    protected void ddPosition_SelectedIndexChanged(object sender, EventArgs e)
    {
        // get reference to the row
        GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
        DropDownList ddPosition = (DropDownList)sender;
        int PlayerID = int.Parse(gvPlayers.DataKeys[gvr.DataItemIndex].Value.ToString());
        int PositionID = int.Parse(ddPosition.SelectedValue);
        Player.UpdatePosition(PlayerID, PositionID);
        BindDisplay();
    }

    protected void gvPlayers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddPosition = (DropDownList)e.Row.Cells[3].FindControl("ddPosition");
            ddPosition.DataSource = dtPositions;
            ddPosition.DataValueField = "PlayerPositionID";
            ddPosition.DataTextField = "Position";
            ddPosition.DataBind();

            if (DataBinder.Eval(e.Row.DataItem, "PositionID") != null)
            {
                ddPosition.SelectedValue = DataBinder.Eval(e.Row.DataItem, "PositionID").ToString();
            }
        }
    }
}