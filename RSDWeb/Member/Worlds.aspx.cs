﻿using RSDLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_Worlds : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindWorlds();
        }
    }

    private void BindWorlds()
    {
        gvMain.DataSource = World.GetOpenWorlds();
        gvMain.DataBind();
    }
}