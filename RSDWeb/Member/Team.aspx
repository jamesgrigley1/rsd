﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Member.master" AutoEventWireup="true" CodeFile="Team.aspx.cs" Inherits="Member_Team" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Panel ID="pnlOpen" runat="server">
        <h1>Team Open</h1>
        <asp:Button ID="btnPurchaseTeam" Text="Purchase Team" OnClick="btnPurchaseTeam_Click" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlEditTeam" runat="server">
        <table>
            <tr>
                <td>League:</td>
                <td><asp:Label ID="lblLeague" runat="server" /></td>
            </tr>
            <tr>
                <td>Team Name:</td>
                <td><asp:TextBox ID="txtTeamName" runat="server" /></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><asp:DropDownList ID="ddCity" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="0" Text="--- Select City ---" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Colors</td>
                <td><asp:DropDownList ID="ddColor1" runat="server" /> <asp:DropDownList ID="ddColor2" runat="server" /></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="btnUpdateTeam" Text="Update Team" OnClick="btnUpdateTeam_Click" runat="server" /></td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlTeam" runat="server">
        <div class="floatleft">
            <h1>Team</h1>
        <table>
            <tr>
                <td width="100">League:</td>
                <td><asp:Label ID="lblLeague_d" runat="server" /></td>
            </tr>
            <tr>
                <td>Team Name:</td>
                <td><asp:Label ID="lblTeamName_d" runat="server" /></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><asp:Label ID="lblCity_d" runat="server" /></td>
            </tr>
            <tr>
                <td>Colors:</td>
                <td></td>
            </tr>
        </table>

        </div>
        <div class="floatleft">
            <h1>Roster</h1>
            <asp:GridView ID="gvPlayers" OnRowDataBound="gvPlayers_RowDataBound" DataKeyNames="PlayerID" runat="server">
                <Columns>
                    <asp:BoundField DataField="PlayerID" HeaderText="No" />
                    <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                    <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                    <asp:TemplateField HeaderText="Position">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddPosition" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddPosition_SelectedIndexChanged" runat="server">
                                <asp:ListItem Value="-1" Text="None" />
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </asp:Panel>
    </asp:Panel>
</asp:Content>

