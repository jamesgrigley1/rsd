﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Member.master" AutoEventWireup="true" CodeFile="Worlds.aspx.cs" Inherits="Member_Worlds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:GridView ID="gvMain" AutoGenerateColumns="false" runat="server">
        <Columns>
            <asp:BoundField DataField="WorldName" HeaderText="World" />
            <asp:BoundField DataField="OpenTeams" HeaderText="Open Teams" />
            <asp:TemplateField>
                <ItemTemplate>
                    <a href="Teams.aspx?w=<%# DataBinder.Eval(Container.DataItem, "WorldID") %>">View</a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

