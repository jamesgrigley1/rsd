﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Member.master" AutoEventWireup="true" CodeFile="Teams.aspx.cs" Inherits="Member_Teams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="floatleft">
        <h1>American League</h1>
        <asp:GridView ID="gvLeague1" AutoGenerateColumns="false" runat="server" Width="400">
            <Columns>
                <asp:BoundField DataField="TeamName" HeaderText="Team Name" />
                <asp:BoundField DataField="CityName" HeaderText="City" />
                <asp:BoundField DataField="Owner" HeaderText="Owner" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="Team.aspx?t=<%# DataBinder.Eval(Container.DataItem, "TeamID") %>">View</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="floatleft">&nbsp;&nbsp;</div>
    <div class="floatleft">
        <h1>National League</h1>
        <asp:GridView ID="gvLeague2" AutoGenerateColumns="false" runat="server" Width="400">
            <Columns>
                <asp:BoundField DataField="TeamName" HeaderText="Team Name" />
                <asp:BoundField DataField="CityName" HeaderText="City" />
                <asp:BoundField DataField="Owner" HeaderText="Owner" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="Team.aspx?t=<%# DataBinder.Eval(Container.DataItem, "TeamID") %>">View</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="clear"></div>
</asp:Content>

