﻿using RSDLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_Teams : System.Web.UI.Page
{
    int WorldID = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["w"] != null)
            WorldID = int.Parse(Request.QueryString["w"].ToString());

        if (!Page.IsPostBack)
        {
            BindTeams();
        }
    }

    private void BindTeams()
    {
        DataView dv = new DataView(Team.GetTeams(WorldID));

        dv.RowFilter = "LeagueTypeID=1";
        gvLeague1.DataSource = dv;
        gvLeague1.DataBind();
        dv.RowFilter = "LeagueTypeID=2";
        gvLeague2.DataSource = dv;
        gvLeague2.DataBind();
    }
}