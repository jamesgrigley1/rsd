﻿using RSDLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Member_FreeAgents : System.Web.UI.Page
{
    private DataSet dsMain;

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (dsMain != null)
        {
            if (dsMain.Tables[0] != null)
            {
                gvPlayers.DataSource = dsMain.Tables[0].DefaultView;
                gvPlayers.DataBind();
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
        else
        {
            if (Session["dsMain_FreeAgents"] != null)
                dsMain = (DataSet)Session["dsMain_FreeAgents"];
        }
    }

    private void BindGrid()
    {
        DataHelper db = new DataHelper("spGetFreeAgents");
        dsMain = db.GetDataSet();
        Session["dsMain_FreeAgents"] = dsMain;

    }

    protected void gvPlayers_SortCommand(object sender, GridViewSortEventArgs e)
    {

        if (dsMain.Tables[0].DefaultView.Sort == e.SortExpression)
        {
            string[] SortExps = e.SortExpression.Split(',');

            if (SortExps[0].IndexOf("ASC") > -1)
                SortExps[0] = SortExps[0].Replace("ASC", "DESC");
            else
                SortExps[0] = SortExps[0].Replace("DESC", "ASC");

            dsMain.Tables[0].DefaultView.Sort = string.Join(",", SortExps);
        }
        else
            dsMain.Tables[0].DefaultView.Sort = e.SortExpression;

        Session["dsMain_FreeAgents"] = dsMain;

    }

    protected void gvPlayers_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = new LinkButton();
            lb = (LinkButton)e.Row.FindControl("lnkHirePlayer");
            lb.CommandArgument = e.Row.RowIndex.ToString();

        }
    }

    protected void gvPlayers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Contains("lnk"))
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (index == -1) return;

            int PlayerID = int.Parse(gvPlayers.DataKeys[index].Values[0].ToString());

            Player.HirePlayer(PlayerID, User.Identity.Name.ToString());
            BindGrid();
        }

    }
}