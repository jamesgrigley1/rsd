﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Member.master" AutoEventWireup="true" CodeFile="FreeAgents.aspx.cs" Inherits="Member_FreeAgents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="floatleft">
        <h1>Free Agents</h1>
        <asp:GridView ID="gvPlayers" DataKeyNames="PlayerID"
            OnSorting="gvPlayers_SortCommand" 
            OnRowCommand="gvPlayers_RowCommand" 
            OnRowCreated="gvPlayers_RowCreated" 
            runat="server">
            <Columns>
                <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName ASC" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName ASC" />
                <asp:BoundField DataField="PlayerStatus" HeaderText="Status" SortExpression="PlayerStatus ASC" />
                <asp:BoundField DataField="BatContact" HeaderText="Bat Contact" SortExpression="BatContact DESC" />
                <asp:BoundField DataField="BatPower" HeaderText="Bat Power" SortExpression="BatPower DESC" />
                <asp:BoundField DataField="PitchAccuracy" HeaderText="Pitch Accuracy" SortExpression="PitchAccuracy DESC" />
                <asp:BoundField DataField="PitchPower" HeaderText="Pitch Power" SortExpression="PitchPower DESC" />
                <asp:TemplateField HeaderText="Actions">
                <ItemStyle HorizontalAlign="center" VerticalAlign="middle" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lnkHirePlayer" CommandName="lnkHirePlayer" Text="Hire Player"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>

