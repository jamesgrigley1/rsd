﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using RSDLib;

public partial class Account_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
    }

    protected void btnLogin_OnClick(object sender, EventArgs e)
    {
        if (Login())
            Response.Redirect("~/");
        else
            FailureText.Text = "Login failed, you suck.";
    }

    private bool Login()
    {
        bool bolOK = false;
        UserAccount user = new UserAccount(txtUserName.Text, txtPassword.Text);

        if (user.UserID > 0){
            Session["UserAccount"] = user;
            FormsAuthentication.RedirectFromLoginPage(user.UserID.ToString(), false);
        }
        return bolOK;
    }

}
