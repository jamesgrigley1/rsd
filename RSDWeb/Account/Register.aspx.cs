﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RSDLib;

public partial class Account_Register : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
    }

    protected void btnCreate_OnClick(object sender, EventArgs e)
    {
        if (Register())
            Response.Redirect("~/");
        else
            ErrorMessage.Text = "Could not add user, it may already exist.";
    }

    private bool Register()
    {
        bool bolOK = false;
        DataHelper db = new DataHelper("spUserAdd");

	    db.Command.Parameters.AddWithValue("@EmailAddress", txtEmail.Text);
	    db.Command.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
	    db.Command.Parameters.AddWithValue("@LastName", txtLastName.Text);
	    db.Command.Parameters.AddWithValue("@UserName", txtUserName.Text);
	    db.Command.Parameters.AddWithValue("@Password", Utility.EncryptPassword(txtPassword.Text));

        foreach (DataRow dr in db.GetDataSet().Tables[0].Rows)
        {
            if ((int)dr["UserID"] > 0)
            {
                FormsAuthentication.SetAuthCookie(dr["UserID"].ToString(), true);
                bolOK = true;
            }
        }
        return bolOK;
    }


}
